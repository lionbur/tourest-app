import React, {Fragment} from 'react'
import { Router, Link } from 'react-static'
import styled, { injectGlobal } from 'styled-components'
import { hot } from 'react-hot-loader'
import {Provider} from 'mobx-react'
import {translate} from 'react-i18next'
import {compose} from 'recompose'
//
import Routes from 'react-static-routes'
import LocaleDetector from 'shared/components/LocaleDetector'
import * as store from 'shared/store'
import 'i18n'
import getDirection from 'shared/translate/direction'

injectGlobal`
  body {
    font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial,
      'Lucida Grande', sans-serif;
    font-weight: 300;
    font-size: 16px;
    margin: 0;
    padding: 0;
  }
`

const AppStyles = styled.div`
  a {
    text-decoration: none;
    color: #108db8;
    font-weight: bold;
  }

  nav {
    width: 100%;
    background: #108db8;

    a {
      color: white;
      padding: 1rem;
      display: inline-block;
    }
  }

  .content {
    padding: 1rem;
  }

  img {
    max-width: 100%;
  }
  
  height: 100%;
  direction: ${({language}) => getDirection(language)};
`

/*
      <nav>
        <Link exact to="/">Home</Link>
        <Link to="/about">About</Link>
        <Link to="/blog">Blog</Link>
      </nav>
 */

const App = ({i18n:{language}}) => (
  <Provider {...store}>
    <Router>
      <Fragment>
        <LocaleDetector/>
        <AppStyles {...{language}}>
          <Routes />
        </AppStyles>
      </Fragment>
    </Router>
  </Provider>
)

const enhance = compose(
  hot(module),
  translate(),
)

export default enhance(App)
