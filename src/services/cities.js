export const getCities = async db =>
  (await db.collection('precalc').doc('main').get()).data().cities

export const getTranslations = async (db, cities) => {
  const byCity = db.collection('precalc').doc('translations').collection('byCity')

  return (await Promise.all(
    cities
      .map(async ({city}) => ({
        city,
        ...((await byCity.doc(city).get()).data()),
      }))
  ))
    .reduce((result, {city, ...translations}) => ({
      ...result,
      [city]: translations,
    }), {})
}