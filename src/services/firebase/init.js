import firebase from 'firebase'
import config from './firebaseConfig.json'

const app = firebase.initializeApp(config)
export default app