import firebase from './init'

firebase.firestore().settings({
  timestampsInSnapshots: true
})
export default firebase.firestore()