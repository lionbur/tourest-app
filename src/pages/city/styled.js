import styled, {injectGlobal} from 'styled-components'

injectGlobal`
  .image-gallery-image {
    height: 100vw;
    max-height: 100%;
    @media (orientation: landscape) {
      max-height: calc(100vh - 150px);
      @media (min-width: 768px) {
        max-height: calc(100vh - 210px);
      }
    }
    
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    
    .image-gallery-description {
      left: 10% !important;
      right: 10%;
    }
    
    img {
      @media (orientation: landscape) {
        width: auto !important;
      }
    }
  }
  
  .image-gallery-thumbnails {
    overflow: hidden;
    padding: 5px 0;
    direction: ltr;
  }
`

export const Main = styled.main`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100%;      
    
  header {
    flex: 0 0 auto;
    z-index: 1;
    
    display: flex;
    flex-direction: column;
    align-items: center;
    position: relative;
    transition: 0.5s;
    min-width: 250px;
    max-width: 500px;
    width: 100%;
    
    * {
      transition: 0.5s;
    }
  
    *:not(:last-child) {
      margin-bottom: 0.5em;
    }
    
    input {
      position: absolute;
      top: 0;
      width: 100%;
      box-sizing: border-box;
      
      padding: 0 20px;
      border: none;
      outline: none;
      box-shadow: 0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.08);
      font: 16px arial,sans-serif;
      line-height: 34px;
      
      &:focus {
        background-color: #ffe;
        box-shadow: 0 4px 4px 0 rgba(0,0,0,0.16), 0 0 0 2px rgba(0,0,0,0.08);
      }
    }

    button {
      -webkit-linear-gradient(top,#f5f5f5,#f1f1f1);
      background-color: #f2f2f2;
      border-radius: 2px;
      border: 1px solid #f2f2f2;
      color: #757575;
      cursor: pointer;
      font-family: arial,sans-serif;
      font-size: 13px;
      font-weight: bold;
      margin: 11px 4px;
      min-width: 54px;
      padding: 16px;
      text-align: center;
      outline: none;
      
      &:hover {
        color: black;
        -webkit-linear-gradient(top,#f5f5f5,#ffffff);
        border: 1px solid #ddd;
        box-shadow: 0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.08);
      }
    }

    img {
      position: absolute;
      z-index: 1;
      width: 32px;
      height: 32px;
      left: 26px;
      top: 0px;
      transform: translateX(-50%);
      pointer-events: none;
    }
    input[value=''] ~ img {
      width: 100px;
      height: 100px;
      left: 50%;
      top: 20px;
    }
    
    div.spacer {
      height: 40px;
      width: 100%;
      pointer-events: none;
    }
    input[value=''] ~ div.spacer {
      height: 160px;
    }
    input[value=''] {
      top: 120px;
    }
    
    div.slogan {
      position: absolute;
      opacity: 0;
      margin-top: -10px;
    }
    input[value=''] ~ div.slogan { 
      opacity: 1;
      margin-top: 10px;
    }
  }
`

export const GallerySection = styled.section`
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  
  display: flex;
  flex-direction: column;
  max-width: 100vw;
  max-height: calc(100vh);
  overflow: hidden;
  
  div.image-gallery, div.image-gallery-content {
    height: 100%;
  }
  div.image-gallery-slide-wrapper.bottom {
    height: calc(100% - 120px);
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`