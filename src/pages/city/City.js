import React, {Fragment} from 'react'
import {compose, lifecycle} from 'recompose'
import {withRouteData, withSiteData} from 'react-static'
import {translate} from 'react-i18next'
import {inject, observer} from 'mobx-react'
import ImageGallery from 'react-image-gallery'
import { uniqBy } from 'lodash'

//
import {Main, GallerySection} from './styled'
import 'react-image-gallery/styles/css/image-gallery.css'

const enhance = compose(
  translate('home'),
  withSiteData,
  withRouteData,
  inject(({search, user}) => ({search, user})),
  lifecycle({
    componentWillMount() {
      this.props.user.locale = this.props.i18n.language.split('-')[0]
      this.props.user.countryCode = this.props.countryCode
      this.props.user.city = this.props.city
    },
  }),
  observer,
)

const getTextContext = (text, keyword) => {
  const words = text.toLowerCase().split(' ')
  const index = words.findIndex(word => word.indexOf(keyword) >= 0)

  if (index < 0) {
    const pos = text.indexOf(keyword)
    const start = text.lastIndexOf(' ', pos - 2)
    const end = text.indexOf(' ', text.indexOf(' ', pos) + 1)
    return text.substring(start, end)
  } else if (index < 3) {
    return words.slice(0, 3).join(' ') + (words.length > 3 ? '...' : '')
  } else if (index < words.length - 3) {
    return '...' + words.slice(index - 1, index + 2).join(' ') + '...'
  } else {
    return '...' + words.slice(index, index + 3).join(' ')
  }
}

const renderItem = ({original}) => (
  <div
    style={{
      width: '100%',
      height: '100%',
      backgroundImage: `url("${original}")`,
    }}
  >
    Test
  </div>
)

const getDisplayLine = ({
  type,
  text,
  item,
  category,
  restaurant,
  keyword
}) => {
  switch (type) {
    case 'item':
      return `${category} - ${text}`
    case 'category':
      return `${category} - ${restaurant}`
    case 'restaurant':
      return restaurant
    case 'itemDescription':
      return `${category} - ${item} - ${getTextContext(text, keyword)}`
    case 'restaurantDescription':
      return `${restaurant} - ${getTextContext(text, keyword)}`
    default:
      return ''
  }
}

export default enhance(({t, i18n, translations, search}) => {
  const isStatic = typeof window === 'undefined'
  const style = {opacity: isStatic ? 0 : undefined}
  const inCity = translations && translations.inCity[i18n.language.split('-')[0]]
  const {suggestedWords, query, suggestedPhrases, otherResults} = search
  const words = (suggestedWords[query] || [])
    .filter(word => word !== query)
  const phrases = (suggestedPhrases[query] || [])
    .sort((a, b) => b.score - a.score)
  const items = (otherResults[query] || [])
    .map(({type, item, category, restaurant, logo, image, ready, text}) => ({
      original: image || logo,
      thumbnail: image || logo,
      description: ready
        ? getDisplayLine({type, item, category, restaurant, text})
        : getTextContext(text, query)
    }))

  return (
    <Fragment>
      <Main
        onSubmit={event => {
          event.preventDefault()
        }}
      >
        <header>
          <input
            autoFocus
            name="query"
            key="input"
            placeholder={isStatic ? '' :t('placeholder') + (inCity ? ` ${inCity}?` :'')}
            value={search.query}
            onChange={event => {
              search.query = event.target.value
            }}
          />
          <div className="slogan" {...{style}}>{t('slogan')}</div>
          <img
            alt="tou.rest"
            src={require('../assets/tourest.svg')}
          />
          <div className="spacer"/>
        </header>
        {words.length > 0 && <section>
          <header>{t('suggestedWords')}</header>
          {words
            .map(word => <div key={word}>{word}</div>)}
        </section>}
        {phrases.length > 0 && <section>
          <header>{t('suggestedPhrases')}</header>
          {phrases
            .map(({text, types}) =>
              <div key={text}>{
                types.some(type => /Description$/.test(type))
                  ? getTextContext(text, query)
                  : text
              }</div>)}
        </section>}
        {items.length > 0 && <GallerySection>
          <ImageGallery
            style={{
              height: '100%',
            }}
            {...{items}}
            autoPlay
            lazyLoad
            showFullscreenButton={false}
          />
        </GallerySection>}
      </Main>
    </Fragment>
  )
})

/*
          {results
            .filter(({results}) => results.length)
            .map(({phrase:{text}, results:[{item, category, restaurant, logo, image, type}]}) => (
              <Fragment key={text+item+category+restaurant}>
                <div>{getDisplayLine({type, text, item, category, restaurant, logo, image, keyword: query})}</div>
              </Fragment>
            ))}

 */