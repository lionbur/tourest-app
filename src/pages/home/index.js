import React, {Fragment} from 'react'
import styled from 'styled-components'
import {compose} from 'recompose'
import {withRouteData, withSiteData} from 'react-static'
import {translate} from 'react-i18next'
//

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  
  padding: 20px;
  
  * {
    transition: 0.5s;
  }
  
  *:not(:last-child) {
    margin-bottom: 0.5em;
  }
  
  input {
    min-width: 250px;
    max-width: 80vw;
    padding: 0 20px;
    border: none;
    outline: none;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.08);
    font: 16px arial,sans-serif;
    line-height: 34px;
    
    &:focus {
      background-color: #ffe;
      box-shadow: 0 4px 4px 0 rgba(0,0,0,0.16), 0 0 0 2px rgba(0,0,0,0.08);
    }
  }
  
  button {
    -webkit-linear-gradient(top,#f5f5f5,#f1f1f1);
    background-color: #f2f2f2;
    border-radius: 2px;
    border: 1px solid #f2f2f2;
    color: #757575;
    cursor: pointer;
    font-family: arial,sans-serif;
    font-size: 13px;
    font-weight: bold;
    margin: 11px 4px;
    min-width: 54px;
    padding: 16px;
    text-align: center;
    outline: none;
    
    &:hover {
      color: black;
      -webkit-linear-gradient(top,#f5f5f5,#ffffff);
      border: 1px solid #ddd;
      box-shadow: 0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.08);
    }
  }
`

const enhance = compose(
  translate('home'),
  withSiteData,
  withRouteData,
)

export default enhance(({t, i18n, translations}) => {
  const isStatic = typeof window === 'undefined'
  const style = {opacity: isStatic ? 0 : 1}
  const inCity = translations && translations.inCity[i18n.language.split('-')[0]]

  return (
    <Fragment>
      <Form onSubmit={event => {
        event.preventDefault()
      }}>
        <img
          width="100"
          height="100"
          alt="tou.rest"
          src={require('../assets/tourest.svg')}
        />
        <div {...{style}}>{t('slogan')}{inCity && `, ${inCity}`}</div>
        <input
          key="input"
          placeholder={isStatic ? '' :t('placeholder')}
        />
        <button disabled={isStatic} {...{style}} type="submit">{t('searchButton')}{inCity && ` ${inCity}`}</button>
      </Form>
    </Fragment>
  )
})
