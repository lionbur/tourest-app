const rtlLanguages = [ 'ar', 'he' ]

export default language => rtlLanguages.includes(language.split('-')[0]) ? 'rtl' : 'ltr'