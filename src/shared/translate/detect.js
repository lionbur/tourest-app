import locale from 'locale2'

export const getLocaleCode = () => process.env.LOCALE
  || (((typeof navigator !== 'undefined') && navigator.languages) || []).filter(lang => !/^en-?/i.test(lang))[0]
  || locale
export default () => getLocaleCode().split('-')[0]