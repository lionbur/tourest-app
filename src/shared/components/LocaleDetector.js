import React, {Component} from 'react'
import {inject} from 'mobx-react'
//
import detectLocale from 'shared/translate/detect'

@inject ('user')
export default class extends Component {
  componentWillMount() {
    this.props.user.locale = detectLocale()
  }

  render () {
    return null
  }
}
