import { observable } from 'mobx'

export default observable({
  locale: null,
  currentPosition: null,
  countryCode: null,
  city: null,
})
