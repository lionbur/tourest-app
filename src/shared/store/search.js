import { observable, autorun, set, get, has, camelCase } from 'mobx'

import user from './user'
import db from 'services/firebase/db'

import sha256 from 'sha256'
import bs58 from 'bs58'

const hash = content => bs58.encode(sha256(Buffer.from(content, 'utf8'), {asBytes: true}))

const search = observable({
  query: '',
  suggestedWords: {},
  suggestedPhrases: {},
  otherResults: {},
})

export default search

const searchPhrases = async ({
  phraseDocs,
  locale,
  otherResults,
  cityDoc,
  query,
}) => {
  const phraseIds = phraseDocs
    .map(({ id }) => id)
  const results = otherResults[query] || []
  const phrasesColl = cityDoc
    .collection('phrases')
    .doc(locale)
    .collection('phrase')
  const indexColl = cityDoc
    .collection('index')
  const dedupItems = {}

  phraseIds.forEach(async id => {
    const [phraseDoc, indexDocs] = await Promise.all([
      phrasesColl.doc(id).get(),
      indexColl
        .doc(id)
        .collection('results')
        .get()
    ])
    const phraseData = phraseDoc.data()
    const chunk = indexDocs
      .docs
      .map(doc => ({
        id: doc.id,
        ready: false,
        ...phraseData,
        ...doc.data(),
      }))
      .filter(({ item }) => {
        if (!item) {
          return true
        }

        if (dedupItems[item]) {
          return false
        }

        dedupItems[item] = true
        return true
      })

    results.push(...chunk)
    set(otherResults, {
      [query]: results,
    })

    const keysByType = {
      item: ['category'],
      itemDescription: ['category', 'item'],
      category: ['restaurant'],
      categoryDescription: ['category'],
      restaurantDescription: ['restaurant'],
      restaurant: [],
    }

    chunk
      .forEach(async result => {
        const keys = keysByType[result.type]
        const found = keys.length
          ? results.find(({ id }) => result.id === id)
          : null

        if (found) {
          await Promise.all(
            keys
              .map(async key => {
                const {text: value} = (await
                    phrasesColl.doc(result[key]).get()
                ).data() || { text: '' }

                found[key] = value
              }))

          found.ready = true
          set(otherResults, {
            [query]: results,
          })
        }
      })
  })
}

autorun(async () => {
  const {countryCode, city, locale} = user
  const {suggestedWords, suggestedPhrases, otherResults} = search
  const query = (search.query || '').toLowerCase()

  if (countryCode && city && locale && query) {
    // /countries/ru/cities/Moscow/words/he/word
    const cityDoc = db
      .collection('countries')
      .doc(countryCode)
      .collection('cities')
      .doc(city)
    const wordColl = cityDoc
      .collection('words')
      .doc(locale)
      .collection('word')

    if (!has(suggestedPhrases, query)) {
      const exactWordDoc = wordColl.doc(hash(query))
      const exactWord = await exactWordDoc.get()

      if (exactWord.exists) {
        if (!has(otherResults, query)) {
          const firstResults = 1

          await searchPhrases({
            phraseDocs: (await exactWordDoc
              .collection('phrases')
              .orderBy('score', 'desc')
              .limit(firstResults)
              .get())
              .docs,
            locale,
            otherResults,
            cityDoc,
            query,
          })
          await searchPhrases({
            phraseDocs: (await exactWordDoc
              .collection('phrases')
              .orderBy('score', 'desc')
              .startAt(firstResults)
              .limit(100)
              .get())
              .docs,
            locale,
            otherResults,
            cityDoc,
            query
          })
        }
      }
    }

    if (!has(suggestedWords, query)) {
      const wordDocs = await wordColl
        .where('word', '>=', query)
        .where('word', '<', query.substring(0, query.length - 1) + String.fromCharCode(query.charCodeAt(query.length - 1) + 1))
        .limit(10)
        .get()

      set(suggestedWords, {
        [query]: wordDocs.docs
          .map(doc => doc.data().word)
      })

      console.log(query, suggestedWords[query])
    }
  }
}, {
  delay: 300,
})