import admin from 'firebase-admin'
import config from './firebaseAdmin.json'

admin.initializeApp({
  credential: admin.credential.cert(config),
  databaseURL: "https://tourest-1.firebaseio.com"
})
export default admin